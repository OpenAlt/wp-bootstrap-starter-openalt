<?php

/* Enqueue parent theme stylesheet */
function wp_bootstrap_starter_parent_css() {
    wp_enqueue_style('wp-bootstrap-starter-parent', get_template_directory_uri().'/style.css');
}
add_action('wp_enqueue_scripts', 'wp_bootstrap_starter_parent_css');

/* Enqueue stylesheet */
function wp_bootstrap_starter_openalt_css() {
    wp_dequeue_style('wp-bootstrap-starter-style');
    wp_enqueue_style('wp-bootstrap-starter-openalt', get_stylesheet_directory_uri().'/style.css');
}
add_action('wp_enqueue_scripts', 'wp_bootstrap_starter_openalt_css', 100);

/* Remove theme customisation options colliding with child stylesheet */
function wp_bootstrap_starter_customize_openalt_deregister($wp_customize) {
    $wp_customize->remove_section('typography');
    set_theme_mod('theme_option_setting', 'sandstone');
    set_theme_mod('preset_style_setting', 'default');

    $wp_customize->remove_section('header_image');
    set_theme_mod('header_banner_visibility', true);

    $wp_customize->remove_section('site_name_text_color');
}
add_action('customize_register', 'wp_bootstrap_starter_customize_openalt_deregister', 100);
function wp_bootstrap_starter_openalt_setup() {
    remove_theme_support('custom-background');
    remove_theme_support('custom-header');
}
add_action('after_setup_theme', 'wp_bootstrap_starter_openalt_setup', 100);

/* Remove extra footer widget areas */
function wp_bootstrap_starter_widgets_openalt_remove() {
    unregister_sidebar('footer-2');
    unregister_sidebar('footer-3');
}
add_action('widgets_init', 'wp_bootstrap_starter_widgets_openalt_remove', 100);

/* Remove non-default templates */
function wp_bootstrap_starter_openalt_remove_templates() {
    return array();
}
add_filter('theme_page_templates', 'wp_bootstrap_starter_openalt_remove_templates');
add_filter('theme_post_templates', 'wp_bootstrap_starter_openalt_remove_templates');
add_filter('theme_attachment_templates', 'wp_bootstrap_starter_openalt_remove_templates');

/* Opt out for suggesting the persistent object cache */
add_filter('site_status_should_suggest_persistent_object_cache', '__return_false');
