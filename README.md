# WP Bootstrap Starter - OpenAlt

Toto je vzhled CMS WordPress pro [stránky spolku OpenAlt z.s.](https://www.openalt.org/).

Vzhled je odvozený ze šablony [WP Bootstrap Starter](https://wordpress.org/themes/wp-bootstrap-starter/), která musí být pro jeho fungování rovněž nainstalována.
